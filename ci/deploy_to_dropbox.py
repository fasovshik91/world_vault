import dropbox
import argparse

class TransferData:
    def __init__(self, app_key, app_secret, refresh_token):
        self.app_key = app_key
        self.app_secret = app_secret
        self.refresh_token = refresh_token

    def upload_file(self, file_from, file_to):
        """upload a file to Dropbox using API v2
        """
        dbx = dropbox.Dropbox(
            app_key=self.app_key,
            app_secret=self.app_secret,
            oauth2_refresh_token=self.refresh_token
        )

        with open(file_from, 'rb') as f:
            dbx.files_upload(f.read(), file_to)


def main():
    parser = argparse.ArgumentParser(description='Upload a file to Dropbox.')
    parser.add_argument('app_key', help='Dropbox app key')
    parser.add_argument('app_secret', help='Dropbox app secret')
    parser.add_argument('refresh_token', help='Dropbox refresh token')
    parser.add_argument('file_from', help='Path to the file to upload')
    parser.add_argument('file_to', help='Path to upload the file to on Dropbox')

    args = parser.parse_args()

    transfer_data = TransferData(args.app_key, args.app_secret, args.refresh_token)

    # API v2
    transfer_data.upload_file(args.file_from, args.file_to)


if __name__ == '__main__':
    main()

