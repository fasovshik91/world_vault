import requests
import argparse


def telegram_send_text(message, token, chat_id):
    send_text = ('https://api.telegram.org/bot' + token + '/sendMessage?chat_id=' + chat_id +
                 '&parse_mode=Markdown&text=' + message)
    response = requests.get(send_text)
    return response.json()


def main():
    parser = argparse.ArgumentParser(description='Upload a file to Dropbox.')
    parser.add_argument('success', help='Pipeline success')
    parser.add_argument('bot_token', help='Telegram bot token')
    parser.add_argument('bot_chatID', help='Telegram bot chat ID')
    parser.add_argument('pipeline_link', help='Pipeline link')
    parser.add_argument('file_name', nargs='?', default='', help='Dropbox file name (optional)')
    args = parser.parse_args()

    if args.success == 'True':
        message = (f'*Pipeline success* \n'
                   f'[pipeline]({args.pipeline_link})\n'
                   f'[Dropbox](https://www.dropbox.com/home/app_python)\n'
                   f'[Download](https://www.dropbox.com/home/app_python?preview={args.file_name})')
    else:
        message = f'*Pipeline failed* \n[pipeline]({args.pipeline_link})'
    telegram_send_text(message, args.bot_token, args.bot_chatID)


if __name__ == '__main__':
    main()
