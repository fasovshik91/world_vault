import json
import os
from kivymd.uix.screen import Screen
from kivy.metrics import dp
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.filemanager import MDFileManager
from kivymd.toast import toast
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton
from jsonschema import validate, ValidationError

class ImportWindow(Screen):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.json_path = ""
        self.dropdown = None
        self.manager_open = False
        self.file_manager = None

    def open_items(self):
        json_content = self.get_json_content()
        deck_names = [deck['name'] for deck in json_content['decks']]
        menu_items = [
            {
                "icon": "git",
                "text": f"{i}",
                "height": dp(56),
                "width": dp(400),
                "on_release": lambda x=f"{i}": self.set_item(x),
            } for i in deck_names
        ]
        self.dropdown = MDDropdownMenu(
            caller=self.ids.drop_item,
            items=menu_items,
            position="bottom",
            width_mult=4,
        )
        self.dropdown.open()

    def set_item(self,text_item):
        self.ids.drop_item.text = text_item
        self.dropdown.dismiss()

    def on_enter(self, *args):
        self.ids.import_button.disabled = True
        self.file_manager = MDFileManager(
            exit_manager=self.exit_manager, select_path=self.select_path
        )

    def import_to_deck(self):

        is_valid, message = self.validate_json(self.ids.deck_path_text_field.text)
        if not is_valid:
            self.show_dialog(message)
            return
        json_content = self.get_json_content()
        cards_of_deck = self.get_cards_of_deck(self.get_json_content(), self.ids.drop_item.text)
        imported_cards_json = self.get_imported_cards_json(self.ids.deck_path_text_field.text)
        identical_cards = list(self.get_identical_cards(cards_of_deck, imported_cards_json))
        if identical_cards:
            self.dialog = MDDialog(title="Успешно",
                                   text=f'Карточки с словом "{', '.join(identical_cards)}" '
                                        f'уже существуют в колоде "{self.ids.drop_item.text}"\n'
                                        f'Заменить карточки в колоде "{self.ids.drop_item.text}"?',
                                   buttons=[
                                       MDFlatButton(text="Да", on_release=lambda *args:
                                       self.delete_identical_cards_from_deck(identical_cards=identical_cards,
                                                                             json_content=json_content,
                                                                             imported_cards_json=imported_cards_json)),
                                       MDFlatButton(text="Нет", on_release=lambda *args:
                                       self.delete_identical_cards_from_imported_cards(identical_cards=identical_cards,
                                                                                       imported_cards_json=imported_cards_json,
                                                                                       json_content=json_content))], )
            self.dialog.open()
            return
        new_json_content = self.update_deck(json_content, self.ids.drop_item.text, imported_cards_json)
        self.save_imported_cards_to_json(new_json_content)

    def delete_identical_cards_from_imported_cards(self, *args, identical_cards, imported_cards_json, json_content):
        new_cards_of_deck = [card for card in imported_cards_json if card['english_word'] not in identical_cards]
        new_json_content = self.update_deck(json_content, self.ids.drop_item.text, new_cards_of_deck)
        self.dismiss_dialog()
        self.save_imported_cards_to_json(new_json_content)


    def delete_identical_cards_from_deck(self, *args, identical_cards, json_content, imported_cards_json):
        for deck in json_content['decks']:
            if deck['name'] == self.ids.drop_item.text:
                # Создаем новый список карточек, не включая те, которые находятся в identical_cards
                deck['cards'] = [card for card in deck['cards'] if card['english_word'] not in identical_cards]
        new_json_content = self.update_deck(json_content, self.ids.drop_item.text, imported_cards_json)
        self.dismiss_dialog()
        self.save_imported_cards_to_json(new_json_content)


    def update_deck(self, json_content, deck_name, cards):
        for deck in json_content['decks']:
            if deck['name'] == deck_name:
                # Добавляем каждую карточку из списка cards в deck['cards'] отдельно
                for card in cards:
                    deck['cards'].append(card)
                break
        return json_content


    def get_identical_cards(self, cards_of_deck, imported_cards_json):
        imported_cards = []
        for card in imported_cards_json:
            imported_cards.append(card['english_word'])
        return list(set(cards_of_deck) & set(imported_cards))



    def get_json_content(self):
        with open(self.json_path, 'r', encoding='utf-8') as f:
            data = json.load(f)
            return data

    def get_imported_cards_json(self, path_to_import_json):
        with open(path_to_import_json, 'r', encoding='utf-8') as f:
            data = json.load(f)
            return data

    def get_cards_of_deck(self, json_content, deck_name):
        cards = []
        for deck in json_content['decks']:
            if deck['name'] == deck_name:
                for card in deck['cards']:
                    cards.append(card['english_word'])
        return cards


    def save_imported_cards_to_json(self, json_content):
        with open(self.json_path, 'w', encoding='utf-8') as file:
            json.dump(json_content, file, ensure_ascii=False, indent=4)
        self.open_dialog_import_cards("Карточки были импортированы в колоду:")

    def select_path(self, path: str):
        '''
        It will be called when you click on the file name
        or the catalog selection button.

        :param path: path to the selected directory or file;
        '''

        self.exit_manager()
        toast(path)
        self.ids.deck_path_text_field.text = path

    def exit_manager(self, *args):
        '''Called when the user reaches the root of the directory tree.'''

        self.manager_open = False
        self.file_manager.close()

    def events(self, instance, keyboard, keycode, text, modifiers):
        '''Called when buttons are pressed on the mobile device.'''

        if keyboard in (1001, 27):
            if self.manager_open:
                self.file_manager.back()
        return True

    def file_manager_open(self):
        self.file_manager.show(os.path.expanduser("~"))  # output manager to the screen
        self.manager_open = True

    def open_dialog_import_cards(self,message):
        self.dialog = MDDialog(title="Успешно",
                               text=f"{message} "
                                    f"{self.ids.drop_item.text}",
                               buttons=[MDFlatButton(text="Ок", on_release=self.open_dialog_back_to_menu), ], )
        self.dialog.open()

    def open_dialog_back_to_menu(self, *args):
        self.manager.current = 'MenuWindow'
        self.manager.transition.direction = "right"
        self.dialog.dismiss()

    def dismiss_dialog(self, *args):
        self.dialog.dismiss()

    def on_leave(self, *args):
        self.clear_fields()
        self.restore_filters()


    def validate_fields(self):
        if self.ids.deck_path_text_field.text == "":
            self.ids.deck_path_text_field.required = True
        if self.ids.drop_item.text == "":
            self.ids.drop_item.required = True

        if (self.ids.deck_path_text_field.text == "" or
                self.ids.drop_item.text == ""):
            self.ids.import_button.disabled = True
        else:
            self.ids.import_button.disabled = False

    def show_dialog(self, message):
        self.dialog = MDDialog(title="Предупреждение",
                               text=message,
                               buttons=[MDFlatButton(text="Ок", on_release=self.dismiss_dialog),], )
        self.dialog.open()
        return True

    def restore_filters(self, *args):
        self.clear_fields()
        self.ids.drop_item.error = False
        self.ids.deck_path_text_field.error = False

    def clear_fields(self):
        fields_to_clear = [
            "drop_item",
            "deck_path_text_field"
        ]
        for field in fields_to_clear:
            self.ids[field].text = ""


    def validate_json(self, json_file):
        with open(json_file, 'r', encoding='utf-8') as file:
            data = json.load(file)

            for entry in data:
                # Проверка наличия всех необходимых полей
                required_fields = ['english_word', 'english_definition', 'russian_translation', 'russian_definition',
                                   'context', 'image_path']
                for field in required_fields:
                    if field not in entry:
                        return False, f"Отсутствует обязательное поле '{field}' в указанном файле : {json_file}"

                # Проверка существования изображения
                image_path = entry['image_path']
                if image_path != '' and not os.path.isfile(image_path):
                    return False, f"Файл изображения '{image_path}' не существует"

        return True, "JSON файл прошел валидацию успешно"


    def show_dialog_error(self, message):
        self.dialog = MDDialog(title="Ошибка",
                               text=message,
                               buttons=[MDFlatButton(text="Ок", on_release=self.dismiss_dialog),], )
        self.dialog.open()
        return True