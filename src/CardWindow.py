import random
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.screen import Screen
from kivy.animation import Animation
from kivy.metrics import dp
from kivy.core.window import Window
import src.CardContextWindow as CardContextWindow
import src.CardMemeWindow as CardMemeWindow
import pkg_resources
import os, sys


class CardWindow(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.card_side = ""
        self.card = {}
        self.animation_direction = ""
        self.target_deck_content = {}
        self.learned_cards = []
        self.animation_spead = 0.3
        self.screen_width = Window.width

    def toggle_interface(self, enable=True):
        if enable:
            self.ids.flip_button.disabled = False
            self.ids.arrow_left_button.disabled = False
            self.ids.arrow_right_button.disabled = False
            self.ids.menu_button.disabled = False
        else:
            self.ids.flip_button.disabled = True
            self.ids.arrow_left_button.disabled = True
            self.ids.arrow_right_button.disabled = True
            self.ids.menu_button.disabled = True

    def show_first_card(self):
        self.ids.arrow_left_button.disabled = True
        card = self.get_random_card(self.target_deck_content, len(self.target_deck_content['cards']))
        self.ids.learning_card.size = (dp(500), dp(300))
        self.ids.count_cards_label.text = f"{len(self.learned_cards)}/{len(self.target_deck_content['cards'])}"
        self.ids.term_label.text = card['english_word']
        print(card['english_word'])
        print(self.ids.term_label.text)
        self.ids.defenetion_label.text = card['english_definition']
        self.card = card
        self.check_image_path()
        self.check_context()

    def show_card(self, direction):
        if direction == "left":
            self.card = self.get_previous_card(self.target_deck_content)
            if self.card is None: #Вы на первой карточке
                return
        else:
            self.card = self.get_random_card(self.target_deck_content, len(self.target_deck_content['cards']))
            if self.card is None: #Все карточки изучены
                self.show_dialog_learn_again("Карточки закончились. \nХотите повторить изучение?")
                return
        self.toggle_interface(enable=False)
        card = self.ids.learning_card
        self.animation_direction = direction
        if direction == "right":
            anim = Animation(pos_hint={"center_x": -2}, duration=self.animation_spead)
            anim.bind(on_complete=self.show_next_card)
        else:
            anim = Animation(pos_hint={"center_x": 2}, duration=self.animation_spead)
            anim.bind(on_complete=self.show_previous_card)
        anim.bind(on_complete=self.move_card_back)
        anim.start(card)

    def move_card_back(self, instance, value):
        card = self.ids.learning_card
        if self.animation_direction == "right":
            card.pos_hint = {"center_x": 2}
            anim = Animation(pos_hint={"center_x": 0.5}, duration=self.animation_spead)

        else:
            card.pos_hint = {"center_x": -2}
            anim = Animation(pos_hint={"center_x": 0.5}, duration=self.animation_spead)
        self.check_image_path()
        self.check_context()
        anim.start(card)

    def show_next_card(self, *args):
        self.card_side = "front"
        self.ids.count_cards_label.text = f"{len(self.learned_cards)}/{len(self.target_deck_content['cards'])}"
        self.ids.term_label.text = self.card['english_word']
        self.ids.defenetion_label.text = self.card['english_definition']
        self.toggle_interface(enable=True)

    def show_previous_card(self, *args):
        self.card_side = "front"
        self.ids.count_cards_label.text = f"{len(self.learned_cards)}/{len(self.target_deck_content['cards'])}"
        self.ids.term_label.text = self.card['english_word']
        self.ids.defenetion_label.text = self.card['english_definition']
        self.toggle_interface(enable=True)
        if len(self.learned_cards) == 1:
            self.ids.arrow_left_button.disabled = True

    def flip_card(self):
        self.toggle_interface(enable=False)
        anim = Animation(pos_hint={"center_x": 0.5, "center_y": 0.5}, duration=0.5)
        self.ids.learning_card.pos_hint = {"center_x": 0.5, "center_y": 0.5}
        anim.start(self.ids.learning_card)
        # Clear labels before starting animation
        self.ids.term_label.text = ""
        self.ids.defenetion_label.text = ""

        # Start animation for increasing size
        anim_flip = Animation(size=(dp(520), dp(0)), duration=0.2)
        anim_flip.bind(on_complete=self.start_flip_animation)  # Bind the next animation
        anim_flip.start(self.ids.learning_card)

    def start_flip_animation(self, *args):
        # Start animation for decreasing size after the first animation completes
        anim_down = Animation(size=(dp(500), dp(300)), duration=0.2)
        anim_down.bind(on_complete=self.set_label_text)
        anim_down.start(self.ids.learning_card)

    def set_label_text(self, *args):
        if self.card_side == "front":
            self.card_side = "back"
            self.ids.term_label.text = self.card['russian_translation']
            self.ids.defenetion_label.text = self.card['russian_definition']
        else:
            self.card_side = "front"
            self.ids.term_label.text = self.card['english_word']
            self.ids.defenetion_label.text = self.card['english_definition']
        self.toggle_interface(enable=True)
        if len(self.learned_cards) == 1:
            self.ids.arrow_left_button.disabled = True


    def get_previous_card(self, deck):
        if len(self.learned_cards) == 1: #Вы на 0 карте
            return None
        self.learned_cards.pop()
        return deck['cards'][self.learned_cards[-1]]

    def open_card_context(self):
        card_context_window = self.manager.get_screen('CardContextWindow')
        card_context_window.context = self.card["context"]
        card_context_window.learned_word = self.card["english_word"]
        card_context_window.show_context()
        card_context_window.ids.toolbar.ids.label_title.font_size = '34sp'
        self.manager.current = 'CardContextWindow'
        self.manager.transition.direction = "down"

    def open_card_meme(self):
        card_meme_window = self.manager.get_screen('CardMemeWindow')
        card_meme_window.meme_path = self.card["image_path"]
        card_meme_window.show_meme()
        card_meme_window.ids.toolbar.ids.label_title.font_size = '34sp'
        self.manager.current = 'CardMemeWindow'
        self.manager.transition.direction = "down"

    def get_random_card(self, deck, count):
        while True:
            random_value = random.randint(0, count - 1)
            if len(self.target_deck_content['cards']) == len(self.learned_cards): #Все карточки изучены
                return None
            if random_value not in self.learned_cards:
                self.learned_cards.append(random_value)
                return deck['cards'][random_value]

    def check_image_path(self):
        if self.card["image_path"] == "":
            self.ids.meme_button.disabled = True
        else:
            self.ids.meme_button.disabled = False

    def check_context(self):
        if self.card["context"] == "":
            self.ids.context_button.disabled = True
        else:
            self.ids.context_button.disabled = False

    def show_dialog_learn_again(self, message):
        self.dialog = MDDialog(title="Предупреждение",
                               text=message,
                               buttons=[MDFlatButton(text="Да", on_release=self.revise_card_again),
                                        MDFlatButton(text="Нет", on_release=self.show_dialog_back_to_menu)], )
        self.dialog.open()
        return True

    def show_dialog_back_to_menu(self, *args):
        self.manager.transition.direction = "left"
        self.manager.current = "MenuWindow"
        self.dismiss_dialog()
    def dismiss_dialog(self, *args):
        self.dialog.dismiss()

    def revise_card_again(self, *args):
        self.learned_cards = []
        self.card_side = "front"
        self.show_first_card()
        self.dismiss_dialog()

    def on_pre_enter(self, *args):
        self.ids.term_label.font_name = self.get_font_path()

    def get_font_path(self):
        if getattr(sys, 'frozen', False):
            return pkg_resources.resource_filename(__name__, 'Roboto-Regular.ttf')
        else:
            current_dir = os.path.abspath(os.path.dirname(__file__))
            return os.path.join(current_dir, 'Roboto-Regular.ttf')
