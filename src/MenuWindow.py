import json
from kivymd.uix.button import MDRoundFlatButton, MDIconButton
from kivymd.uix.screen import Screen
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
import src.ExportWindow as ExportWindow
import src.EditDeckWindow as EditDeckWindow
from kivy.metrics import dp
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.button import MDRectangleFlatButton,MDFloatingActionButton, MDRectangleFlatIconButton, MDRoundFlatIconButton, MDFillRoundFlatIconButton
from kivy.uix.behaviors import ButtonBehavior
from kivymd.uix.dropdownitem import MDDropDownItem

class MenuWindow(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.json_path = "decks_test.json"
        self.theme_cls = None
        self.buttons = []
        self.icon_buttons = []


    def load_decks_buttons(self, data):
        self.buttons = []
        self.icon_buttons = []
        self.ids.toolbar.ids.label_title.font_size = '34sp'
        self.ids.containerGridLayout.clear_widgets()
        for deck in data['decks']:
            button = MDRoundFlatIconButton(
                icon="cards",
                text=f"{deck['name']}",
                font_size="20sp",
                size_hint=(0.5, 0.5),
                line_color=self.theme_cls.primary_color,
                halign="left"
            )
            button.bind(on_press=lambda instance, name=deck['name']: self.open_card_window(data, name))
            self.ids.containerGridLayout.add_widget(button)
            edit_deck_icon_button = MDIconButton(
                icon="square-edit-outline",
                icon_size="30sp",
                theme_icon_color="Custom",
                icon_color=self.theme_cls.primary_color,
            )
            self.ids.containerGridLayout.add_widget(edit_deck_icon_button)
            edit_deck_icon_button.bind(
                on_press=lambda instance, name=deck['name']: self.open_edit_deck_window(data, name))
            delete_deck_icon_button = MDIconButton(
                icon="delete",
                icon_size="30sp",
                theme_icon_color="Custom",
                icon_color=self.theme_cls.primary_color,
            )
            self.buttons.append(button)
            self.icon_buttons.append(edit_deck_icon_button)
            self.icon_buttons.append(delete_deck_icon_button)
            self.ids.containerGridLayout.add_widget(delete_deck_icon_button)
            delete_deck_icon_button.bind(
                on_press=lambda instance, name=deck['name']: self.open_dialog_delete_deck(name))

    def open_card_window(self, data, button_name):
        json_content = self.get_json_content()
        deck_content = self.set_target_deck(json_content, button_name)
        if not deck_content['cards']:
            self.show_dialog_empty_deck("В этом колоде нет карточек")
            return
        card_window = self.manager.get_screen('CardWindow')
        card_window.target_deck_content = deck_content
        card_window.learned_cards = []
        card_window.card_side = "front"
        card_window.show_first_card()
        card_window.ids.toolbar.ids.label_title.font_size = '34sp'
        self.manager.transition.direction = "left"
        self.manager.current = 'CardWindow'

    def open_edit_deck_window(self, data, button_name):
        edit_deck_window = self.manager.get_screen('EditDeckWindow')
        edit_deck_window.json_path = self.json_path
        edit_deck_window.deck_name = button_name
        edit_deck_window.show_table()
        edit_deck_window.theme_cls = self.theme_cls
        self.manager.current = 'EditDeckWindow'
        self.manager.transition.direction = "left"


    @staticmethod
    def set_target_deck(data, deck_name):
        for deck in data['decks']:
            if deck['name'] == deck_name:
                return deck

    def get_json_content(self):
        with open(self.json_path, 'r', encoding='utf-8') as f:
            data = json.load(f)
        return data

    def open_dialog_delete_deck(self, deck_name):
        self.dialog = MDDialog(title="Успешно",text=f"Вы уверены что хотите удалить колоду {deck_name}",
                               buttons=[MDFlatButton(text="Да",on_release=lambda *args: self.delete_deck(deck_name)),
                                        MDFlatButton(text="Нет",on_release=self.dismiss_dialog)],)
        self.dialog.open()

    def dismiss_dialog(self, *args):
        self.dialog.dismiss()

    def delete_deck(self, deck_name, *args):
        json_content = self.get_json_content()
        for deck in json_content['decks']:
            if deck['name'] == deck_name:
                json_content['decks'].remove(deck)
                break
        self.sync_json(json_content)
        self.load_decks_buttons(self.get_json_content())
        self.dialog.dismiss()

    def sync_json(self, data):
        with open(self.json_path, 'w', encoding='utf-8') as file:
            json.dump(data, file, ensure_ascii=False, indent=4)

    def show_dialog_empty_deck(self, message):
        self.dialog = MDDialog(title="Предупреждение",
                               text=message,
                               buttons=[MDFlatButton(text="Ок", on_release=self.dismiss_dialog),], )
        self.dialog.open()
        return True


    def open_export_window(self):
        export_window = self.manager.get_screen('ExportWindow')
        export_window.json_path = self.json_path
        export_window.ids.toolbar.ids.label_title.font_size = '34sp'
        self.manager.current = 'ExportWindow'
        self.manager.transition.direction = "left"

    def open_import_window(self):
        import_window = self.manager.get_screen('ImportWindow')
        import_window.json_path = self.json_path
        import_window.ids.toolbar.ids.label_title.font_size = '34sp'
        self.manager.current = 'ImportWindow'
        self.manager.transition.direction = "left"

    def open_create_deck_window(self):
        create_deck_window = self.manager.get_screen('CreateDeckWindow')
        create_deck_window.json_path = self.json_path
        create_deck_window.ids.toolbar.ids.label_title.font_size = '34sp'
        self.manager.current = 'CreateDeckWindow'
        self.manager.transition.direction = "left"


    def switch_theme_style(self):
        self.theme_cls.primary_palette = (
            "Orange" if self.theme_cls.primary_palette == "Blue" else "Blue"
        )
        self.theme_cls.theme_style = (
            "Dark" if self.theme_cls.theme_style == "Light" else "Light"
        )
        self.load_decks_buttons(self.get_json_content())

    def open_items(self):
        menu_items = [
            {
                "icon": "git",
                "text": f"{i}",
                "height": dp(56),
                "width": dp(400),
                "on_release": lambda x=f"{i}": self.set_item(x),
            } for i in ["Светло-синия", "Светло-зеленая", "Темно-оранжевая", "Темно-серая"]
        ]

        # Получаем доступ к кнопке из right_action_items
        right_action_button = self.ids.toolbar.ids.right_actions.children[0]

        # Используем кнопку из right_action_items как caller для выпадающего меню
        self.dropdown = MDDropdownMenu(
            caller=right_action_button,
            items=menu_items,
            width_mult=4,
        )
        self.dropdown.open()

    def set_item(self, item_name):
        if item_name == "Светло-синия":
            self.theme_cls.theme_style = "Light"
            self.theme_cls.primary_palette = "Blue"
        elif item_name == "Светло-зеленая":
            self.theme_cls.theme_style = "Light"
            self.theme_cls.primary_palette = "Teal"
        elif item_name == "Темно-оранжевая":
            self.theme_cls.theme_style = "Dark"
            self.theme_cls.primary_palette = "Orange"
        elif item_name == "Темно-серая":
            self.theme_cls.theme_style = "Dark"
            self.theme_cls.primary_palette = "BlueGray"
        for btn in self.buttons:
            btn.line_color = self.theme_cls.primary_color
        for icon in self.icon_buttons:
            icon.icon_color=self.theme_cls.primary_color
        self.dropdown.dismiss()
