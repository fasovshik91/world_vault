import json
from kivymd.uix.screen import Screen
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton

class CreateDeckWindow(Screen):
    def __init__(self, **kwargs):
        super(CreateDeckWindow, self).__init__(**kwargs)
        self.json_path = ""

    def add_new_deck(self):
        if self.check_same_deck_name(self.ids.deck_name_text_field.text):
            self.show_dialog("Предупреждение","Такая колода уже существует")
            return
        new_deck = self.ids.deck_name_text_field.text
        json_file = "decks_test.json"
        new_deck = {
            "name": new_deck,
            "cards": []
        }
        with open(json_file, 'r', encoding='utf-8') as f:
            data = json.load(f)
        data['decks'].append(new_deck)
        with open(json_file, 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)

        main_menu_window = self.manager.get_screen('MenuWindow')
        main_menu_window.load_decks_buttons(main_menu_window.get_json_content())
        self.show_dialog("Информация", f"Колода {self.ids.deck_name_text_field.text} успешно создана")
        self.manager.current = 'MenuWindow'
        self.manager.transition.direction = "right"

    def restore_filters(self, *args):
        self.clear_fields()
        self.ids.deck_name_text_field.error = False
    def clear_fields(self):
        fields_to_clear = [
            "deck_name_text_field",
        ]
        for field in fields_to_clear:
            self.ids[field].text = ""
    def on_leave(self, *args):
        self.restore_filters()
    def on_enter(self, *args):
        self.ids.add_deck_button.disabled = True

    def validate_fields(self):
        if self.ids.deck_name_text_field.text == "":
            self.ids.deck_name_text_field.required = True

        if (self.ids.deck_name_text_field.text == "" or
                len(self.ids.deck_name_text_field.text) > self.ids.deck_name_text_field.max_text_length):
            self.ids.add_deck_button.disabled = True
        else:
            self.ids.add_deck_button.disabled = False

    def check_same_deck_name(self, deck_name):
        json_content = self.get_json_content()
        for deck in json_content['decks']:
            if deck['name'] == deck_name:
                return True
        return False

    def get_json_content(self):
        with open(self.json_path, 'r', encoding='utf-8') as f:
            data = json.load(f)
        return data

    def show_dialog(self, title, message):
        self.dialog = MDDialog(title=title,
                               text=message,
                               buttons=[MDFlatButton(text="Ок", on_release=self.dismiss_dialog),], )
        self.dialog.open()
        return True

    def dismiss_dialog(self, *args):
        self.dialog.dismiss()