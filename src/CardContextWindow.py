from kivymd.uix.screen import Screen
import re


class CardContextWindow(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.context = ""
        self.learned_word = ""

    def show_context(self):
        self.ids.context_label.markup = True
        self.learned_word = self.learned_word.replace(" ", "")
        learned_word = self.learned_word
        index = self.learned_word.find("[")
        if index != -1:
            learned_word = self.learned_word[:index]
        print(learned_word)
        formatted_text = re.sub(
            re.escape(learned_word),
            lambda match: f"[b]{match.group()}[/b]",
            self.context,
            flags=re.IGNORECASE
        )
        self.ids.context_label.text = formatted_text
