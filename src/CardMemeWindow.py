import json
import random
from kivymd.app import MDApp
from kivymd.uix.button import MDRoundFlatButton, MDIconButton
from kivy.lang import Builder
from kivymd.uix.screenmanager import MDScreenManager
from kivymd.uix.screen import Screen
from kivy.animation import Animation
from kivy.metrics import dp

class CardMemeWindow(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.meme_path = ""

    def show_meme(self):
        self.ids.meme_image.source = self.meme_path
