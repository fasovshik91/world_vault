import json
import os
from kivymd.uix.screen import Screen
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton
from kivymd.uix.filemanager import MDFileManager
import pkg_resources
import os, sys


class EditCardWindow(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.json_path = ""
        self.deck_name = ""
        self.card_name = ""

    def update_card_in_deck(self):
        if self.ids.english_word.text != self.card_name and self.check_uniq_card():
            self.show_dialog("Карточка с такими словом уже существует")
            self.ids.english_word.error = True
            return
        if self.ids.image_path_text_field.text != "" and not self.image_path_validation():
            self.show_dialog("Неверный путь к картинке")
            return
        json_content = self.get_json_content()
        for deck in json_content['decks']:
            if deck['name'] == self.deck_name:
                for card in deck['cards']:
                    if card['english_word'] == self.card_name:
                        card.update(self.get_content_for_changed_card())
                        break
        self.sync_json(json_content)
        self.dialog = MDDialog(title="Успешно", text="Карточка успешно изменена",
                               buttons=[MDFlatButton(text="Окей", on_release=self.back_to_edit_deck_window_dialog)], )
        self.dialog.open()

    def back_to_edit_deck_window_dialog(self, *args):
        self.dismiss_dialog()
        self.back_to_edit_deck_window()
        self.manager.current = 'EditDeckWindow'
        self.manager.transition.direction = "right"

    def dismiss_dialog(self, *args):
        self.dialog.dismiss()

    def get_content_for_changed_card(self):
        return {
            "english_word": self.ids.english_word.text,
            "english_definition": self.ids.english_definition.text,
            "russian_translation": self.ids.russian_translation.text,
            "russian_definition": self.ids.russian_definition.text,
            "context": self.ids.context.text,
            "image_path": self.ids.image_path_text_field.text
        }

    def get_json_content(self):
        with open(self.json_path, 'r', encoding='utf-8') as file:
            data = json.load(file)
            return data

    def sync_json(self, data):
        with open(self.json_path, 'w', encoding='utf-8') as file:
            json.dump(data, file, ensure_ascii=False, indent=4)

    def fill_fields(self):
        json_content = self.get_json_content()
        target_card = self.find_target_card(json_content)
        self.ids.english_word.text = target_card['english_word']
        self.ids.english_definition.text = target_card['english_definition']
        self.ids.russian_translation.text = target_card['russian_translation']
        self.ids.russian_definition.text = target_card['russian_definition']
        self.ids.context.text = target_card['context']
        self.ids.image_path_text_field.text = target_card['image_path']

    def back_to_edit_deck_window(self):
        edit_deck_window = self.manager.get_screen('EditDeckWindow')
        edit_deck_window.show_table()
        self.manager.current = 'EditDeckWindow'
        self.manager.transition.direction = "right"

    def find_target_card(self, json_content):
        for deck in json_content['decks']:
            if deck['name'] == self.deck_name:
                for card in deck['cards']:
                    if card['english_word'] == f"{self.card_name}":
                        return card
    #validation
    def validate_fields(self):
        if self.ids.english_word.text == "":
            self.ids.english_word.required = True
        if self.ids.english_definition.text == "":
            self.ids.english_definition.required = True
        if self.ids.russian_translation.text == "":
            self.ids.russian_translation.required = True
        if self.ids.russian_definition.text == "":
            self.ids.russian_definition.required = True
        if self.ids.context.text == "":
            self.ids.context.required = True

        if (self.ids.english_word.text == "" or len(self.ids.english_word.text) > self.ids.english_word.max_text_length or
                self.ids.english_definition.text == "" or len(self.ids.english_definition.text) > self.ids.english_definition.max_text_length or
                self.ids.russian_translation.text == "" or len(self.ids.russian_translation.text) > self.ids.russian_translation.max_text_length or
                self.ids.russian_definition.text == "" or len(self.ids.russian_definition.text) > self.ids.russian_definition.max_text_length or
                self.ids.context.text == "" or len(self.ids.context.text) > self.ids.context.max_text_length):
            self.ids.edit_button.disabled = True
        else:
            self.ids.edit_button.disabled = False

    def restore_filters(self, *args):
        self.ids.english_word.error = False
        self.ids.english_definition.error = False
        self.ids.russian_translation.error = False
        self.ids.russian_definition.error = False
        self.ids.context.error = False
        self.ids.image_path_text_field.error = False

    def on_enter(self, *args):
        self.file_manager = MDFileManager(
            exit_manager=self.exit_manager, select_path=self.select_path
        )

    def select_path(self, path: str):
        self.exit_manager()
        self.ids.image_path_text_field.text = path

    def exit_manager(self, *args):
        self.manager_open = False
        self.file_manager.close()

    def events(self, instance, keyboard, keycode, text, modifiers):
        if keyboard in (1001, 27):
            if self.manager_open:
                self.file_manager.back()
        return True

    def on_leave(self, *args):
        self.restore_filters()

    def file_manager_open(self):
        self.file_manager.show(os.path.expanduser(os.getcwd()))  # output manager to the screen
        self.manager_open = True

    def image_path_validation(self):
        return os.path.isfile(self.ids.image_path_text_field.text) and any(
            self.ids.image_path_text_field.text.endswith(extension) for extension in
            ['.jpg', '.jpeg', '.png', '.gif', '.bmp', '.webp'])

    def check_uniq_card(self):
        json_content = self.get_json_content()
        for deck in json_content["decks"]:
            if deck["name"] == self.deck_name:
                for card in deck["cards"]:
                    if card['english_word'] == self.ids.english_word.text:
                        return True
        return False

    def show_dialog(self, message):
        self.dialog = MDDialog(title="Предупреждение",
                               text=message,
                               buttons=[MDFlatButton(text="Ок", on_release=self.dismiss_dialog),], )
        self.dialog.open()
        return True

    def on_pre_enter(self, *args):
        self.ids.english_word.font_name = self.get_font_path()

    def get_font_path(self):
        if getattr(sys, 'frozen', False):
            return pkg_resources.resource_filename(__name__, 'Roboto-Regular.ttf')
        else:
            current_dir = os.path.abspath(os.path.dirname(__file__))
            return os.path.join(current_dir, 'Roboto-Regular.ttf')

