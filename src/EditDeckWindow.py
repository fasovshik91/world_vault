import re
import json
from kivy.clock import Clock
from kivymd.uix.screen import Screen
from kivy.metrics import dp
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton
from kivymd.uix.datatables import MDDataTable
from kivymd.uix.button import MDRoundFlatButton, MDIconButton
import src.AddNewCardWindow as AddNewCardWindow
import src.EditCardWindow as EditCardWindow
import pkg_resources
import os, sys


class EditDeckWindow(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.json_path = ""
        self.deck_name = ""
        self.cards_check_list = []
        self.target_deck = {}
        self.is_filtered = False
        self.rows = []
        self.theme_cls = None
        self.cancel_icon_button = None
        self.data_table = None

    def show_table(self):
        self.cards_check_list = []
        self.ids.topAppTextField.text = f"{self.deck_name}"
        self.ids.topAppTextField.width = dp(len(self.deck_name) * 14)
        self.ids.topAppEditButton.icon = "pencil"
        self.target_deck = self.get_target_deck(self.get_json_content(), self.deck_name)
        self.ids.containerBoxLayout.clear_widgets()

        cards = self.target_deck['cards']
        if not self.is_filtered:
            self.rows = [(f"[font={self.get_font_path()}]{card['english_word']}[/font]", card['russian_translation']) for card in cards]
        self.data_table = MDDataTable(pos_hint={"center_y": 0.5, "center_x": 0.5}, check=True, rows_num=len(self.rows),
                                      column_data=[("Слово", dp(65)), ("Определение", dp(65)), ], row_data=self.rows,
                                      elevation=5)
        self.data_table.bind(on_check_press=self.on_check_press)
        self.data_table.bind(on_row_press=self.on_row_press)
        self.ids.containerBoxLayout.add_widget(self.data_table)

    def filter_table(self, text):
        self.target_deck = self.get_target_deck(self.get_json_content(), self.deck_name)
        if self.ids.searchTextField.text.strip() == "":
            self.is_filtered = False
            self.show_table()
            return
        filtered_data = []
        for row in self.target_deck['cards']:
            english_word = str(row['english_word']).lower()
            russian_translation = str(row['russian_translation']).lower()
            if text.lower() in english_word or text.lower() in russian_translation:
                filtered_data.append((row['english_word'], row['russian_translation']))
        self.rows = filtered_data
        self.is_filtered = True
        self.show_table()

    def open_dialog_delete_card(self):
        if self.validation_dialog_card_not_selected("Вабирите карточки для удаления"):
            return
        self.dialog = MDDialog(title="Успешно",
                               text=f"Вы уверены что хотите удалить следующие карточки:\n"
                                    f"[font={self.get_font_path()}]{'\n'.join(self.cards_check_list)}[/font]",
                               buttons=[MDFlatButton(text="Да", on_release=self.delete_card),
                                        MDFlatButton(text="Нет", on_release=self.dismiss_dialog)], )
        self.dialog.open()


    def dismiss_dialog(self, *args):
        self.dialog.dismiss()

    def delete_card(self, *args):
        json_content = self.get_json_content()
        for deck in json_content['decks']:
            if deck['name'] == self.deck_name:
                for card in list(deck['cards']):
                    if card['english_word'] in self.cards_check_list:
                        deck['cards'].remove(card)
        self.sync_json(json_content)
        self.filter_table(self.ids.searchTextField.text)
        self.dialog.dismiss()

    def get_json_content(self):
        with open(self.json_path, 'r', encoding='utf-8') as file:
            data = json.load(file)
            return data

    def sync_json(self, data):
        with open(self.json_path, 'w', encoding='utf-8') as file:
            json.dump(data, file, ensure_ascii=False, indent=4)



    @staticmethod
    def get_target_deck(data, deck_name):
        for deck in data['decks']:
            if deck['name'] == deck_name:
                return deck

    def open_edit_card_window(self):
        if self.validation_dialog_card_not_selected("Вабирите карточку для редактирования"):
            return
        if self.validation_dialog_card_more_than_one("Для редактирования выберете только одну карточку"):
            return
        edit_card_window = self.manager.get_screen('EditCardWindow')
        edit_card_window.json_path = self.json_path
        edit_card_window.deck_name = self.deck_name
        edit_card_window.card_name = self.cards_check_list[0]
        edit_card_window.ids.toolbar.ids.label_title.font_size = '34sp'
        edit_card_window.fill_fields()
        self.manager.current = 'EditCardWindow'
        self.manager.transition.direction = "left"

    def open_add_new_card_window(self):
        add_new_card_window = self.manager.get_screen('AddNewCardWindow')
        add_new_card_window.json_path = self.json_path
        add_new_card_window.deck_name = self.deck_name
        add_new_card_window.ids.toolbar.ids.label_title.font_size = '34sp'
        self.manager.current = 'AddNewCardWindow'
        self.manager.transition.direction = "left"

    def validation_dialog_card_not_selected(self, message):
        if len(self.cards_check_list) < 1:
            self.dialog = MDDialog(title="Предупреждение",
                                   text=message,
                                   buttons=[MDFlatButton(text="Ок", on_release=self.dismiss_dialog),], )
            self.dialog.open()
            return True
    def validation_dialog_card_more_than_one(self, message):
        if len(self.cards_check_list) != 1:
            self.dialog = MDDialog(title="Предупреждение", text=message,
                                   buttons=[MDFlatButton(text="Ок", on_release=self.dismiss_dialog)], )
            self.dialog.open()
            return True

    def start_edit_deck_name(self):
        if self.ids.topAppEditButton.icon == "check":
            if self.check_same_deck_name(self.deck_name, self.ids.topAppTextField.text):
                self.validation_dialog_card_not_selected("Такое название колоды уже существует")
                return
            self.ids.topAppEditButton.icon = "pencil"
            self.ids.topAppTextField.readonly = True
            self.ids.topAppTextField.line_color_normal = (0, 0, 0, 0)
            self.ids.topAppTextField.text_color_normal = (1, 1, 1, 1)
            self.edit_deck_name(self.deck_name, self.ids.topAppTextField.text)
            self.deck_name = self.ids.topAppTextField.text
            self.ids.topAppBoxLayout.remove_widget(self.cancel_icon_button)
            main_menu_window = self.manager.get_screen('MenuWindow')
            main_menu_window.load_decks_buttons(main_menu_window.get_json_content())
        elif self.ids.topAppEditButton.icon == "pencil":
            self.ids.topAppTextField.readonly = False
            self.ids.topAppTextField.text_color_normal = (0.9, 0.9, 0.9, 1)
            self.ids.topAppEditButton.icon = "check"
            self.cancel_icon_button = MDIconButton(
                icon="cancel",
                size_hint=(None, None), size=(48, 48), pos_hint={"center_x": .5, "center_y": .5},
            )
            self.ids.topAppBoxLayout.add_widget(self.cancel_icon_button)
            self.cancel_icon_button.bind(on_release=self.on_cancel_button_pressed)
            self.edit_deck_name(self.deck_name, self.ids.topAppTextField.text)
            self.deck_name = self.ids.topAppTextField.text


    def edit_deck_name(self, old_deck_name, new_deck_name):
        json_content = self.get_json_content()
        for deck in json_content['decks']:
            if deck['name'] == old_deck_name:
                deck['name'] = new_deck_name
        self.sync_json(json_content)

    def dynamically_size_text_field(self):
        self.ids.topAppTextField.width = dp(len(self.ids.topAppTextField.text) * 14)

    def on_cancel_button_pressed(self, instance):
        self.cancel_edit_deck_name()

    def cancel_edit_deck_name(self):
        self.ids.topAppEditButton.icon = "pencil"
        self.ids.topAppTextField.readonly = True
        if self.theme_cls.theme_style == "Light":
            self.ids.topAppTextField.text_color_normal = (1, 1, 1, 1)
        else:
            self.ids.topAppTextField.text_color_normal = (0, 0, 0, 1)
        self.ids.topAppTextField.line_color_normal = (0, 0, 0, 0)
        self.ids.topAppTextField.text = self.deck_name
        self.ids.topAppTextField.width = dp(len(self.deck_name) * 14)
        self.ids.topAppBoxLayout.remove_widget(self.cancel_icon_button)

    def check_same_deck_name(self, old_deck_name,deck_name):
        json_content = self.get_json_content()
        for deck in json_content['decks']:
            if deck['name'] == deck_name and old_deck_name != deck_name:
                return True
        return False

    def on_leave(self, *args):
        if self.ids.topAppEditButton.icon == "check":
            self.cancel_edit_deck_name()

    def on_row_press(self, instance_table, instance_row):
        '''Called when a table row is clicked.'''
        index = instance_row.index
        cols_num = len(instance_table.column_data)
        row_num = int(index / cols_num)
        col_num = index % cols_num
        cell_row = instance_table.table_data.view_adapter.get_visible_view(row_num * cols_num)
        if cell_row.ids.check.state == 'normal':
            # instance_table.table_data.select_all('normal')
            # cell_row.ids.check.state = 'down'
            cell_row.change_check_state_no_notify("down")
        else:
            # cell_row.ids.check.state = 'normal'
            cell_row.change_check_state_no_notify("normal")
        instance_table.table_data.on_mouse_select(instance_row)
        ind = instance_row.index // 2  # number of rows
        row_data = instance_table.row_data[ind][0]
        print(f"Row {ind}: {row_data}")  # print the row_data)
        font_path = self.get_font_path()
        pt = r"\[font={}\](.*)\[/font\]".format(re.escape(font_path))
        pattern = rf'{pt}'
        match = re.search(pattern, row_data).group(1)
        print(match)
        if match in self.cards_check_list:
            self.cards_check_list.remove(match)
        else:
            self.cards_check_list.append(match)
        print(self.cards_check_list)

    def on_check_press(self, instance_table, current_row):
        instance_table.unbind(on_row_press=self.on_row_press)
        Clock.schedule_once(self.binding_event_data_table_row_press, 0.5)
        font_path = self.get_font_path()
        pt = r"\[font={}\](.*)\[/font\]".format(re.escape(font_path))
        pattern = rf'{pt}'
        match = re.search(pattern, current_row[0]).group(1)
        if match in self.cards_check_list:
            self.cards_check_list.remove(match)
        else:
            self.cards_check_list.append(match)

    def binding_event_data_table_row_press(self, dt=0):
        self.data_table.bind(on_row_press=self.on_row_press)

    def on_pre_enter(self, *args):
        if self.theme_cls.theme_style == "Light":
            self.ids.topLabel.text_color = (1, 1, 1, 1)
            self.ids.topAppTextField.text_color_normal = (1, 1, 1, 1)
        else:
            self.ids.topLabel.text_color = (0, 0, 0, 1)
            self.ids.topAppTextField.text_color_normal = (0, 0, 0, 1)


    def get_font_path(self):
        if getattr(sys, 'frozen', False):
            return pkg_resources.resource_filename(__name__, 'Roboto-Regular.ttf')
        else:
            current_dir = os.path.abspath(os.path.dirname(__file__))
            return os.path.join(current_dir, 'Roboto-Regular.ttf')