import json
import os
from kivymd.uix.screen import Screen
from kivy.metrics import dp
from kivymd.uix.menu import MDDropdownMenu
from kivymd.uix.filemanager import MDFileManager
from kivymd.toast import toast
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton

class ExportWindow(Screen):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.json_path = ""
        self.dropdown = None
        self.manager_open = False
        self.file_manager = None

    def open_items(self):
        json_content = self.get_json_content()
        deck_names = [deck['name'] for deck in json_content['decks']]
        menu_items = [
            {
                "icon": "git",
                "text": f"{i}",
                "height": dp(56),
                "width": dp(400),
                "on_release": lambda x=f"{i}": self.set_item(x),
            } for i in deck_names
        ]
        self.dropdown = MDDropdownMenu(
            caller=self.ids.drop_item,
            items=menu_items,
            position="bottom",
            width_mult=4,
        )
        self.dropdown.open()

    def set_item(self,text_item):
        self.ids.drop_item.text = text_item
        self.dropdown.dismiss()

    def on_enter(self, *args):
        self.ids.export_button.disabled = True
        self.file_manager = MDFileManager(
            exit_manager=self.exit_manager, select_path=self.select_path
        )

    def export_to_json(self):
        if not os.path.exists(self.ids.deck_path_text_field.text):
            self.show_dialog("Указанной папки не существует")
            return
        deck_name = self.ids.drop_item.text
        path_to_export_json = self.ids.deck_path_text_field.text
        deck_content = self.get_deck_content(deck_name)
        self.save_exported_cards_to_json(deck_content, path_to_export_json, deck_name)

    def get_deck_content(self, deck_name):
        with open(self.json_path, 'r', encoding='utf-8') as f:
            data = json.load(f)
            for deck in data['decks']:
                if deck['name'] == deck_name:
                    return deck['cards']

    def save_exported_cards_to_json(self, deck_content, path_to_json, deck_name):
        with open(path_to_json + f"{deck_name}.json", 'w', encoding='utf-8') as file:
            json.dump(deck_content, file, ensure_ascii=False, indent=4)
        self.open_dialog_export_cards("Карточки были экспортированы в файл")

    def get_json_content(self):
        with open(self.json_path, 'r', encoding='utf-8') as f:
            data = json.load(f)
        return data

    def select_path(self, path: str):
        '''
        It will be called when you click on the file name
        or the catalog selection button.

        :param path: path to the selected directory or file;
        '''

        self.exit_manager()
        toast(path)
        self.ids.deck_path_text_field.text = os.path.join(path, "")

    def exit_manager(self, *args):
        '''Called when the user reaches the root of the directory tree.'''

        self.manager_open = False
        self.file_manager.close()

    def events(self, instance, keyboard, keycode, text, modifiers):
        '''Called when buttons are pressed on the mobile device.'''

        if keyboard in (1001, 27):
            if self.manager_open:
                self.file_manager.back()
        return True

    def file_manager_open(self):
        self.file_manager.show(os.path.expanduser("~"))  # output manager to the screen
        self.manager_open = True

    def on_leave(self, *args):
        self.clear_fields()
        self.restore_filters()


    def open_dialog_export_cards(self, message):
        self.dialog = MDDialog(title="Успешно",
                               text=f"{message}\n"
                                    f"{os.path.join(self.ids.deck_path_text_field.text, "")}"
                                    f"{self.ids.drop_item.text}.json",
                               buttons=[MDFlatButton(text="Ок", on_release=self.open_dialog_back_to_menu), ], )
        self.dialog.open()

    def open_dialog_back_to_menu(self, *args):
        self.manager.current = 'MenuWindow'
        self.manager.transition.direction = "right"
        self.dialog.dismiss()

    def dismiss_dialog(self, *args):
        self.dialog.dismiss()

    def validate_fields(self):
        if self.ids.deck_path_text_field.text == "":
            self.ids.deck_path_text_field.required = True
        if self.ids.drop_item.text == "":
            self.ids.drop_item.required = True

        if (self.ids.deck_path_text_field.text == "" or
                self.ids.drop_item.text == "" or len(self.ids.drop_item.text) > self.ids.drop_item.max_text_length):
            self.ids.export_button.disabled = True
        else:
            self.ids.export_button.disabled = False

    def show_dialog(self, message):
        self.dialog = MDDialog(title="Предупреждение",
                               text=message,
                               buttons=[MDFlatButton(text="Ок", on_release=self.dismiss_dialog),], )
        self.dialog.open()
        return True

    def restore_filters(self, *args):
        self.clear_fields()
        self.ids.drop_item.error = False
        self.ids.deck_path_text_field.error = False

    def clear_fields(self):
        fields_to_clear = [
            "drop_item",
            "deck_path_text_field"
        ]
        for field in fields_to_clear:
            self.ids[field].text = ""