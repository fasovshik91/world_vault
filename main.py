import json
from kivymd.app import MDApp
from kivymd.uix.button import MDRoundFlatButton, MDIconButton
from kivy.lang import Builder
from kivymd.uix.screenmanager import MDScreenManager
from kivymd.uix.snackbar import Snackbar
from kivymd.uix.screen import Screen
import src.CardWindow as CardWindow
import src.MenuWindow as MenuWindow
import src.ImportWindow as ImportWindow
import src.CreateDeckWindow as CreateDeckWindow
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
import os, sys

# Assuming all .kv files are in same directory as this file
KV_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

class WindowManager(MDScreenManager):
    pass


class MainApp(MDApp):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.json_path = "decks_test.json"
    def build(self):
        self.theme_cls.theme_style_switch_animation = True
        # self.theme_cls.theme_style_switch_animation_duration = 0.8
        Builder.load_file(os.path.join(KV_DIRECTORY, "main.kv"))


    def on_start(self):
        self.title = "VaultWorld"
        try:
            data = self.validation_json(self.json_path)
        except Exception as e:
            error_dialog = MDDialog(
                title="Ошибка",
                text=f"Произошла ошибка: {e}",
                size_hint=(0.7, 0.3),
                buttons = [
                    MDFlatButton(
                        text="Закрыть",
                        theme_text_color="Custom",
                        text_color=self.theme_cls.primary_color,
                        on_release=lambda *args: MDApp.get_running_app().stop()
                    ),
                ],
            )
            error_dialog.open()
            return

        self.root.get_screen('MenuWindow').json_path = self.json_path

        main_menu_window = self.root.get_screen('MenuWindow')
        main_menu_window.theme_cls = self.theme_cls
        main_menu_window.load_decks_buttons(data)




    @staticmethod
    def validation_json(file_path):
        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                data = json.load(file)

                # Проверка наличия корневого ключа "decks"
                if 'decks' not in data:
                    print("Отсутствует корневой ключ 'decks'")
                    raise ValueError(f"Отсутствует корневой ключ 'decks'")

                # Проверка наличия подключей для каждой колоды
                for deck in data['decks']:
                    if 'name' not in deck or 'cards' not in deck:
                        print(f"Отсутствует один из обязательных ключей для колоды: 'name' или 'cards'")
                        raise ValueError(f"Отсутствует один из обязательных ключей для колоды: 'name' или 'cards'")

                    # Проверка наличия всех обязательных полей в картах
                    for card in deck['cards']:
                        required_fields = ['english_word', 'english_definition', 'russian_translation',
                                           'russian_definition', 'context', 'image_path']
                        missing_fields = [field for field in required_fields if field not in card]
                        if missing_fields:
                            print(f"В карте отсутствуют обязательные поля: {', '.join(missing_fields)}")
                            raise ValueError(f"В карте отсутствуют обязательные поля: {', '.join(missing_fields)}")

                return data

        except FileNotFoundError:
            print("Указанный файл не найден")
            raise ValueError(f"Указанный файл не найден")
        except json.JSONDecodeError:
            print("Некорректный формат JSON файла")
            raise ValueError(f"Некорректный формат JSON файла")


if __name__ == "__main__":
    MainApp().run()
