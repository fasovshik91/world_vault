# -*- mode: python ; coding: utf-8 -*-
import sys
import os
from kivy.resources import resource_add_path
from kivy_deps import sdl2, glew
from kivymd import hooks_path as kivymd_hooks_path
from PyInstaller.utils.hooks import (
    collect_data_files,
    copy_metadata,
    collect_submodules
)

datas = copy_metadata('kivymd')
hiddenimports = collect_submodules('kivymd')

datas = collect_data_files('kivymd')

path = os.path.abspath(".")
a = Analysis(
    ["main.py"],
    datas=[('main.kv', '.')],
    pathex=[path],
    hookspath=[kivymd_hooks_path],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=None,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=None)
font_files = [('src\\Roboto-Regular.ttf', 'src\\Roboto-Regular.ttf', 'DATA')]

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas + font_files,
    *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
    debug=False,
    strip=False,
    upx=True,
    name="VaultWorld",
    console=False,
    icon='src\\logo_vw.png'
)